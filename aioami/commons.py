from uuid import uuid4
from collections import defaultdict


class AmiProtocolAlfabet:
    EOL = '\r\n'
    STRIP_CHARS = b' ', b'\t'
    EMPTY_BUFFER = END_SIGN = ''


class PublisherSubscriber:
    _default_group = uuid4()

    def __init__(self):
        self._handlers = defaultdict(list)

    def on(self, etype, handler, *, group=None, hid=None):
        hid = hid or uuid4()
        group = group or self._default_group
        
        self._handlers[group] += (etype, handler, hid),
        return hid

    def off(self, *, group=None, hid=None):

        if group is None and hid is None:
            raise ValueError('group or/and hid must be not None')

        if group:
            try:
                del self._handlers[group]
            except KeyError:
                pass

        if hid:
            for group_, handlers in self._handlers.items():
                for etype, handler, hid_ in handlers[:]:
                    if hid_ == hid:
                        self._handlers[group_].remove((etype, handler, hid_))
        return self

    def fire(self, event):
        for handlers in self._handlers.values():
            for etype, handler, hid in handlers:
                if isinstance(event, etype):
                    handler(event)

    @property
    def subscribers_len(self):
        return self.size

    @property
    def size(self):
        return sum(len(handlers) for handlers in self._handlers.values())

    class Event:
        pass
