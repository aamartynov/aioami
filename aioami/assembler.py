from .commons import PublisherSubscriber


class AbstractAssembler(PublisherSubscriber):
    def __call__(self):
        raise NotImplementedError()


class SimpleAssembler(AbstractAssembler):
    def __call__(self, data):
        return self._assemble(data)

    def _assemble(self, data):
        return ('\r\n'.join(f'{key}: {value}' for key, value in data.items()) + '\r\n' * 2).encode()
