import asyncio
from collections import namedtuple
from . import protocol
from .commons import PublisherSubscriber

ConnectionParameters = namedtuple('ConnectionParameters', ['host', 'port', 'user', 'secret'])


class Manager(PublisherSubscriber):
    _connection_parameters = None
    _protocol_factory = None
    _ami_protocol = None

    def __init__(self, host, port, user, secret, protocol_factory=protocol.Protocol):
        PublisherSubscriber.__init__(self)
        self._connection_parameters = ConnectionParameters(host, port, user, secret)
        self._protocol_factory = protocol_factory

    def connect(self):
        task = asyncio.Task(asyncio.get_event_loop().create_connection(
            self._protocol_factory,
            self._connection_parameters.host,
            self._connection_parameters.port
        ))
        task.add_done_callback(self._on_connection_made)
        return task

    def _on_connection_made(self, task):
        _transport, self._ami_protocol = task.result()
        self._ami_protocol.on(protocol.GreetingRecive, self._on_greeting)

    def _on_greeting(self, event):
        print(event.greeting)
        self._login()

    def _login(self):
        self._ami_protocol.send({
            'Action': 'Login',
            'ActionID': 1,
            'Username': self._connection_parameters.user,
            'Secret': self._connection_parameters.secret
        })
