from .commons import PublisherSubscriber
from .event import GreetingRecive, PacketRecive, ResponseRecive, EventRecive
from .error import ParseError


# NOTE - rename Disassembler -> reader and Assembler -> writer
class AbstractDisassembler(PublisherSubscriber):
    _buffer = None
    _eop = None
    _eoplen = None
    _charset = None

    def __call__(self, data):
        raise NotImplementedError('__call__ method must be implemented')

    def __init__(self, eop, charset='utf-8'):
        PublisherSubscriber.__init__(self)
        
        self._buffer = bytearray()
        self._charset = charset
        self._eop = eop
        self._eoplen = len(eop)

    def _put_data(self, data):
        self._buffer.extend(data)

    def _parse_packets(self):
        while True:
            try:
                eopstart = self._buffer.index(self._eop)
            except ValueError:
                return
            
            eopend = eopstart + self._eoplen
            packet = self._buffer[:eopend].strip(self._eop)
            
            del self._buffer[:eopend]
            
            if packet:
                yield self._parse_packet(packet)

    def _parse_packet(self, data):
        raise NotImplementedError('_parse_packet method must be implement')


class GreetingDisassembler(AbstractDisassembler):
    """ For read just single greeting packet """

    def __init__(self):
        super().__init__(b'\r\n')

    def __call__(self, data):
        self._put_data(data)
        packets = list(self._parse_packets())
        size = len(packets)
            
        if size != 1:
            raise ParseError('greeting packet must be one, {} found'.format(size))

        self.fire(GreetingRecive(*packets))

    # override
    def _parse_packet(self, data):
        greeting, version = data.decode(self._charset).split('/', 1)
        return greeting, version


class PacketDisassembler(AbstractDisassembler):
    def __init__(self):
        super().__init__(b'\r\n\r\n')
    
    def __call__(self, data):
        self._put_data(data)
        for packet in self._parse_packets():
            self.fire(PacketRecive(packet))
            self.fire(self._resolve_event_by_packet(packet))
    
    # override
    def _parse_packet(self, data):
        entrys = (entry.decode(self._charset).split(':', 1) for entry in data.split(b'\r\n'))
        return {key.strip(): value.strip() for key, value in entrys}

    def _resolve_event_by_packet(self, packet):
        if 'Response' in packet.keys():
            return ResponseRecive(packet)

        if 'Event' in packet.keys():
            return EventRecive(packet)

        return UnknownRecive(packet)
