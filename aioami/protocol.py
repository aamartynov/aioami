import asyncio

from .commons import PublisherSubscriber
from .assembler import SimpleAssembler
from .disassembler import GreetingDisassembler, PacketDisassembler
from .event import (GreetingRecive, ConnectionLostEvent,
                    PacketRecive, EventRecive,
                    ResponseRecive, UnknownRecive)
from .commons import PublisherSubscriber


class AmiProtocol(PublisherSubscriber):
    """ Independent of asyncio AMI implementation """

    _assembler = None
    _disassembler = None
    
    _is_connected = False
    _is_get_hello = False
    _is_logged = False
    _server = None

    def __init__(self):
        PublisherSubscriber.__init__(self)
        self._init_state()

    def _assemble(self, data):
        return self._assembler(data)

    def _disassemble(self, data):
        self._disassembler(data)

    def _init_state(self):
        self._assembler, self._disassembler = SimpleAssembler(), GreetingDisassembler()
        self._disassembler.on(GreetingRecive, self._on_greeting_recive)
        
    def _on_greeting_recive(self, event):
        self._disassembler = PacketDisassembler()
        # TODO - maybe create method for delegate event
        # like this - `self.delegate_event(self._disassembler, PacketRecive)

        self._disassembler.on(PacketRecive, self._on_packet_recive)
        self._disassembler.on(EventRecive, self._on_event_recive)
        self._disassembler.on(ResponseRecive, self._on_response_recive)
        self._disassembler.on(UnknownRecive, self._on_unknown_recive)
        self.fire(event)

    def _on_packet_recive(self, event):
        return

    def _on_event_recive(self, event):
        print(event.packet)

    def _on_response_recive(self, event):
        print(event.packet)

    def _on_unknown_recive(self, event):
        print(event.packet)


# NOTE - maybe move this definition into manager.py?
class Protocol(asyncio.Protocol, AmiProtocol):
    """ asyncio protocol implementation """
    
    _transport = None

    def connection_made(self, transport):
        self._transport = transport

    def data_received(self, data):
        self._disassemble(data)

    def connection_lost(self, exception):
        self.fire(ConnectionLostEvent())

    def send(self, action):
        self._transport.write(self._assemble(action))
