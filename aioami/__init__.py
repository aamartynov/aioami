import asyncio

from . protocol import Protocol
from .manager import Manager
from .event import EventRecive


def run():
    manager = Manager('sip2.eyekraft.ru', 5038, 'prg10h', '%s3c%prg_10%')
    loop = asyncio.get_event_loop()
    loop.run_until_complete(manager.connect())
    loop.run_forever()
    loop.close()
