from .commons import PublisherSubscriber


class ConnectionLostEvent(PublisherSubscriber.Event): pass

class ConnectionMadeEvent(PublisherSubscriber.Event): pass


class PacketRecive(PublisherSubscriber.Event):
    _packet = None
    
    def __init__(self, packet):
        self._packet = packet

    @property
    def packet(self):
        return self._packet


class EventRecive(PacketRecive): pass

class ResponseRecive(PacketRecive): pass

class UnknownRecive(PacketRecive): pass


class GreetingRecive(PublisherSubscriber.Event):
    _greeting = None

    def __init__(self, greeting):
        self._greeting = greeting

    @property
    def greeting(self):
        return self._greeting
