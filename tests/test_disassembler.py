import os
import pytest

from aioami.disassembler import (AbstractDisassembler,
                                 PacketDisassembler,
                                 GreetingDisassembler)

this_dir = os.path.dirname(os.path.abspath(__file__))


@pytest.fixture
def ami_input_stream():
    """ ami-input-stream.data contain 5 packets """

    with open(os.path.join(this_dir, 'ami-input-stream-5-packets.data'), 'rb') as file:
        return file.read().replace(b'\n', b'\r\n')


class SimpleDisassembler(AbstractDisassembler):

    def __init__(self):
        super().__init__(b'\r\n\r\n')
        self._packets = []

    def __call__(self, data):
        self._put_data(data)
        self._packets += self._parse_packets()

    def _parse_packet(self, data):
        return data

    @property
    def packets(self):
        return self._packets


def test_abstract_disassembler(ami_input_stream):
    disassembler = SimpleDisassembler()

    for chunk in ami_input_stream[:123], ami_input_stream[123:]:
        disassembler(chunk)

    # check packets number
    assert len(disassembler.packets) == 17


def test_packet_disassembler():
    return


def test_greeting_disassembler():
    return


def test_simple_asassembler():
    return
