from setuptools import setup
from setuptools.command.test import test as TestCommand
import sys


dev_requires = ['pytest']


class Test(TestCommand):
    _args = ['tests', '-s']

    def run_tests(self):
        import pytest

        if pytest.main(self._args) > 0:
            sys.exit(0)


setup(
    name='aioami',
    version='0.0.0',
    extras_require={
        'dev': dev_requires
    },
    packages=['aioami'],
    cmdclass={'test': Test}
)
